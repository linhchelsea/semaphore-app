import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import mainMenuStyle from "../../style/mainMenuStyle";
import {removeToken} from "../../stores/authStore";
import { i18n } from '../../locales/I18n';

export default class MainMenu extends Component {
  static navigationOptions = {
    header: null,
  };

  render () {
    return (
      <View style={ mainMenuStyle.container }>
        <TouchableOpacity style={ mainMenuStyle.btn } onPress={() => { this.props.navigation.push('Introduce') }}>
          <Text style={ mainMenuStyle.txtBtn }>{ i18n.t('introduce_') }</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ mainMenuStyle.btn } onPress={ this.onLearn }>
          <Text style={ mainMenuStyle.txtBtn }>{ i18n.t('learn_') }</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ mainMenuStyle.btn }>
          <Text style={ mainMenuStyle.txtBtn }
                onPress={this.onPractice}>{ i18n.t('practice_') }</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ mainMenuStyle.btn }>
          <Text style={ mainMenuStyle.txtBtn }>{ i18n.t('game_') }</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ mainMenuStyle.btn }>
          <Text style={ mainMenuStyle.txtBtn }
                onPress={this.onGame}>{ i18n.t('2_players_game') }</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ mainMenuStyle.btn }>
          <Text style={ mainMenuStyle.txtBtn }>{ i18n.t('rate_this_app') }</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={ mainMenuStyle.btn }
          onPress={ this.onLogout }
        >
          <Text style={ mainMenuStyle.txtBtn }>{ i18n.t('logout_') }</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onLogout = async () => {
    await removeToken(null)
    this.props.navigation.navigate('Login')
  }
  onPractice = () => {
    this.props.navigation.navigate('PracticeMenu')
  };

  onLearn = () => {
    this.props.navigation.push('Learn');
  }

  onGame = () => {
    this.props.navigation.push('Game');
  }
}