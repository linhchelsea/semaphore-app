import { API_URL, DEFAULT_RESPONSE } from '../config/global';
import { getToken } from '../stores/authStore'

export const registerEmail = async (email) => {
  const response = await fetch(`${API_URL}/api/register-email`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email,
    }),
  });
  return response ? JSON.parse(response._bodyInit) : DEFAULT_RESPONSE;
};

export const signUp = async ({ email, password, passwordConfirm, code }) => {
  const response = await fetch(`${API_URL}/api/sign-up`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email,
      code,
      password,
      password_confirm: passwordConfirm,
    }),
  });
  return response ? JSON.parse(response._bodyInit) : DEFAULT_RESPONSE;
};

export const getPractice = async ({ level }) => {
  const token = await getToken();
  console.log(token)
  const response = await fetch(`${API_URL}/api/practices?level=${level}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`
    }
  });
  return response ? JSON.parse(response._bodyInit) : DEFAULT_RESPONSE;
};

export const signIn = async ({ email, password }) => {
  const response = await fetch(`${API_URL}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email,
      password,
    }),
  });
  return response ? JSON.parse(response._bodyInit) : DEFAULT_RESPONSE;
};
