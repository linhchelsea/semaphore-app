import React, { Component } from 'react';
import { View, Text, Image, ProgressBarAndroid, TouchableOpacity, Alert } from 'react-native';
import { getPractice } from '../../services/AuthService';
import { i18n } from '../../locales/I18n';

let listData = [];
let maxLengthData = 0;
let index = 0;
const chars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
let interval = null;
let alert = null;

class Game extends Component {
  static navigationOptions = {header : null}

  constructor (props) {
    super(props);
    this.state = {
      data: [],
      image: '',
      button1: '',
      button2: '',
      button3: '',
      button4: '',
      progress: 0,
      dapan: '',
      point1: 0,
      point2: 0,
    }
  }

  async componentDidMount () {
    listData = [];
    maxLengthData = 0;
    index = 0;
    await this.getData();
    this.getCurrentData(0);
  }

  getData = async () => {
    const data = [];
    while(data.length < 9) {
      const index = Math.floor(Math.random() * chars.length);
      if (data.indexOf(chars[index]) < 0) data.push(chars[index]);
    }
    this.setState({ data });
  }

  getCurrentData (current) {
    const arr = this.getRandomChar(this.state.data[current]);
    this.setState({
      dapan: this.state.data[current],
      button1: arr[0],
      button2: arr[1],
      button3: arr[0],
      button4: arr[1],
      current: current + 1,
    })
  }
  getRandomChar = (char) => {
    const indexChar = chars.indexOf(char);
    let index = Math.floor(Math.random() * chars.length);
    while (index === indexChar) {
      index = Math.floor(Math.random() * chars.length);
    }
    const char2 = chars[index];
    const random = Math.floor(Math.random() * 2);
    return random ? [char, char2] : [char2, char];
  }

  onConfirm = () => {
    if (this.state.current === this.state.data.length) {
      let userWin = '1';
      if (this.state.point1 < this.state.point2) userWin = '2';
      else if (this.state.point1 === this.state.point2) userWin = 'draw';
      Alert.alert(i18n.t('game_'), `User win: ${userWin}`,
        [
          {text: i18n.t('ok'), onPress: () => this.props.navigation.goBack(), style: 'cancel'},
        ],
        { cancelable: false }
      );
      return;
    }
    this.getCurrentData(this.state.current);
  }
  pressButton1 = () => {
    clearInterval(interval);
    if (this.state.button1 === this.state.dapan) {
      this.setState({ point1: this.state.point1 + 1});
    } else {
      this.setState({ point2: this.state.point2 + 1});
    }
    this.onConfirm();
  }
  pressButton2 = () => {
    clearInterval(interval)
    if (this.state.button2 === this.state.dapan) {
      this.setState({ point1: this.state.point1 + 1});
    } else {
      this.setState({ point2: this.state.point2 + 1});
    }
    this.onConfirm();
  }

  pressButton3 = () => {
    clearInterval(interval)
    if (this.state.button3 === this.state.dapan) {
      this.setState({ point2: this.state.point2 + 1});
    } else {
      this.setState({ point1: this.state.point1 + 1});
    }
    this.onConfirm();
  }

  pressButton4 = () => {
    clearInterval(interval)
        clearInterval(interval)
    if (this.state.button4 === this.state.dapan) {
      this.setState({ point2: this.state.point2 + 1});
    } else {
      this.setState({ point1: this.state.point1 + 1});
    }
    this.onConfirm();
  }

  render () {
    return (
      <View style={{ flex: 1 }}>
        <View style={{flex: 1.7, width: '100%', flexDirection: 'row'}}>
          <TouchableOpacity
            style={{
              flex : 4,
              width: '50%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ffee70'
            }}
            onPress={this.pressButton3}>
            <Text style={{fontSize: 30, transform: [{ rotate: '180deg'}]}}> {this.state.button3} </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex : 2,
              width: '50%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#42ff96'
            }}
          >
            <Text style={{fontSize: 30, transform: [{ rotate: '180deg'}]}}> {this.state.point2} - {this.state.point1} </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flex : 4,
              width: '50%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#ff001c'
            }}
            onPress={this.pressButton4}>
            <Text style={{fontSize: 30, transform: [{ rotate: '180deg'}]}}> {this.state.button4} </Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{flex: 7, width: '60%', height: '60%', transform: [{ rotate: '180deg'}]}}
            source={{uri: `https://res.cloudinary.com/dicproyxc/image/upload/v1544609787/${this.state.data[this.state.current-1]}.png`}}
            resizeMode="contain"
          />
          <ProgressBarAndroid
            style={{flex: 0.3, width: '100%', justifyContent: 'flex-end'}}
            styleAttr="Horizontal"
            indeterminate={false}
            progress={this.state.progress}
          />
        </View>
        <View style={{flex: 5, justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{flex: 7, width: '60%', height: '60%'}}
            source={{uri: `https://res.cloudinary.com/dicproyxc/image/upload/v1544609787/${this.state.data[this.state.current-1]}.png`}}
            resizeMode="contain"
          />
        </View>
          <View style={{flex: 1.7, width: '100%', flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                flex : 4,
                width: '50%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ffee70'
              }}
              onPress={this.pressButton1}>

              <Text style={{fontSize: 30}}> {this.state.button1} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex : 2,
                width: '50%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#42ff96'
              }}
            >
              <Text style={{fontSize: 30}}> {this.state.point1} - {this.state.point2} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex : 4,
                width: '50%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ff001c'
              }}
              onPress={this.pressButton2}>
              <Text style={{fontSize: 30}}> {this.state.button2} </Text>
            </TouchableOpacity>
          </View>
        </View>
    )
  }
}

export default Game