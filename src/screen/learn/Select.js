import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList } from 'react-native';
import mainMenuStyle from "../../style/mainMenuStyle";
import {getListLearning} from "../../services/LearnService";

export default class Select extends Component {
  state = { data: [] };
  static navigationOptions = {
    header: null,
  };

  async componentDidMount () {
    const data = await getListLearning();
    this.setState({ data: data.data });
  }
  render () {
    return (
      <View
        style={ mainMenuStyle.container }
      >
        <FlatList
          style={ mainMenuStyle.list }
          data={ this.state.data }
          renderItem={ this.renderItem }
          keyExtractor={ this.keyExtractor }
        />
      </View>
    );
  }

  keyExtractor = item => item.id.toString();

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={ mainMenuStyle.btn }
        onPress={ () => this.props.navigation.push('LearnGroup', { item }) }
      >
          <Text style={ mainMenuStyle.txtBtn }>{ item.name }</Text>
      </TouchableOpacity>
    )
  }
}