import { StyleSheet } from 'react-native';
import {getHeight, getWidth} from "../util/Size";

export default practiceStyle = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgView: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
   buttonView: {
      flex: 1,
      flexDirection : "row",
      justifyContent: 'center',
      alignItems: 'center',
    },
      btnAgain: {
        width: getWidth(50),
        height: getHeight(8),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0300ff',
      },
  btnFinish: {
    width: getWidth(50),
    height: getHeight(8),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
  },
  txt: {
    color: '#ffffff',
    fontSize: 20,
  },
  txtHint: {
    fontSize: 15,
    color: '#6c6c6c',
  },
  txtCharacter: {
    width: getWidth(50),
    height: getHeight(6),
    borderWidth: 1,
    borderColor: 'black',
    textAlign: 'center',
    marginTop: 8,
    marginBottom: 8,
  },
  imgCharacter: {
    width: getWidth(60),
    height: getWidth(60),
  },
});