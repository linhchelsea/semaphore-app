import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { getToken } from '../stores/authStore';

export default class Switch extends Component {
  static navigationOptions = {
    header: null,
  };

  async componentDidMount () {
    const token = await getToken();
    const screen = token ? 'Menu' : 'Login';
    this.props.navigation.push(screen, {});
      setTimeout(() => {
          SplashScreen.hide();
      }, 1500);
  }

  render () {
    return null;
  }
}