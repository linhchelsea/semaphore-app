import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import registerStyle from "../../style/registerStyle";
import RegisterEmail from "./RegisterEmail";
import RegisterVerifyCode from "./RegisterVerifyCode";
import { actionRegisterEmailThunk, actionSetVerifyWarning } from '../../redux/auth/Action';

class Register extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor (props) {
    super(props);
  }

  render () {
    if (this.props.isSendCode) {
      return (
        <View style={ registerStyle.container }>
          <RegisterVerifyCode
            email={ this.props.email }
            navigation={ this.props.navigation }
            verifyCodeWarning={ this.props.verifyCodeWarning }
            passwordWarning={ this.props.passwordWarning }
            passwordConfirmWarning={ this.props.passwordConfirmWarning }
          />
        </View>
      );
    }
    return (
      <View style={ registerStyle.container }>
        <RegisterEmail
          registerEmail={ this.props.actionRegisterEmailThunk }
          setVerifyWarning={ this.props.actionSetVerifyWarning }
          verifyWarning={ this.props.verifyWarning }
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isSendCode: state.auth.isSendCode,
    verifyWarning: state.auth.verifyWarning,
    email: state.auth.email,
    verifyCodeWarning: state.auth.verifyCodeWarning,
    passwordWarning: state.auth.passwordWarning,
    passwordConfirmWarning: state.auth.passwordConfirmWarning,
  }
};

const mapActionToProps = { actionRegisterEmailThunk, actionSetVerifyWarning };

export default connect(mapStateToProps, mapActionToProps)(Register);