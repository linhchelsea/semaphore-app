import { StyleSheet } from 'react-native';
import {getHeight, getWidth} from "../util/Size";

export default loginStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#1175a0'
  },
  mainView: {
    flex: 3,
    justifyContent: 'center',
  },
  bottomView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLoginView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLogin: {
    width: getWidth(80),
    height: getHeight(8),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1175a0',
    borderWidth: 1,
    borderColor: '#ffffff',
    borderRadius: 6,
  },
  txtBtnLogin: {
    color: '#ffffff',
    fontSize: 20,
  },
  txtLink: {
    color: '#ffffff',
    fontSize: 15,
    fontStyle: 'italic',
    textDecorationLine: 'underline'
  },
  txtInput: {
    width: getWidth(80),
    height: getHeight(8),
    textAlign: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 6,
    marginTop: 4,
    marginBottom: 4,
  },
  imgLogo: {
    width: 130,
    height: 130,
    borderRadius: 80,
  },
  txtWarning: {
    fontSize: 12,
    color: '#ffffff'
  },
  title: {
    fontSize: 30,
    color: '#f2f3f4'
  }
});