import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import MainContainer from './router/MainRouter';
type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <MainContainer/>
    );
  }
}
