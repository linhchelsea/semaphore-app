import { AsyncStorage } from 'react-native';

export const getToken = () => {
  return AsyncStorage.getItem('token');
};

export const setToken = (token) => {
  AsyncStorage.setItem('token', token);
};

export const removeToken = () => {
  AsyncStorage.removeItem('token');
};