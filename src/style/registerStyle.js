import { StyleSheet } from 'react-native';
import {getHeight, getWidth} from "../util/Size";

export default loginStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1175a0'
  },
  itemView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  btnLoginView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    width: getWidth(80),
    height: getHeight(8),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1175a0',
    borderWidth: 1,
    borderColor: '#ffffff',
    borderRadius: 6,
  },
  txtBtnLogin: {
    color: '#ffffff',
    fontSize: 20,
  },
  txtInput: {
    width: getWidth(80),
    height: getHeight(8),
    textAlign: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 6,
    marginTop: 4,
    marginBottom: 4,
  },
  txtWarning: {
    fontSize: 12,
    color: '#ffffff'
  }
});