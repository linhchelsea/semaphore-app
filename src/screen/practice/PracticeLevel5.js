import React, { Component } from 'react'
import { View, Text, Image, ProgressBarAndroid, TouchableOpacity, Alert, TextInput } from 'react-native'
import { getPractice } from '../../services/AuthService'
import style from '../../style/practiceStyle'
import { i18n } from '../../locales/I18n'

let listData = []
let maxLengthData = 0
let index = 0
let intervalImage = null
let intervalProgress = null
let alert = null
class PracticeLevel5 extends Component {
  static navigationOptions = {title: i18n.t('level5')}
  constructor (props) {
    super(props)
    this.state = {
      image: '',
      button1: '',
      button2: '',
      progress: 0,
      result : '',
      dapan : ''
    }
  }

  componentDidMount () {

    listData = []
    maxLengthData = 0
    index = 0
    this.getData()
  }

  getData = async () => {
    this.setState({
      progress: 0
    })
    const data = await getPractice({
      level: 5,
    })
    if (data.status === 200) {
      listData = data.data
      maxLengthData = listData.length
      index = 0
      this.setViewLevel(index)
      console.log(listData)
    } else {
      alert('error')
    }
  }

  setViewLevel = (i) => {
    const arrayData = listData[i].text.split('')
    this.setState({
      result : listData[i].text
    })
    let j = 0
    const maxLengthString = arrayData.length-1
    intervalImage = setInterval(() => {
      this.setState({
        image: arrayData[j].toUpperCase()
      }, () => {
        if (j<maxLengthString){
          j++
        } else {
          clearInterval(intervalImage)
          intervalProgress = setInterval(() => {
            var progress = (this.state.progress + 0.00027777777 ) //10/3600(60*60)
            if (this.state.progress <= 1) {
              this.setState({progress: progress})
            } else {
              clearInterval(intervalProgress)
             this.onConfirm()
            }
          }, 1)
        }
      })
    }, 3000)
  }

  onConfirm = () => {
    if (this.state.dapan === this.state.result) {
      if(index<maxLengthData-1){
        index++
        clearInterval(intervalImage)
        clearInterval(intervalProgress)
        this.setState({
          progress : 0,
          dapan : '',
          image : ''
        })
        this.setViewLevel(index)
      } else {
        Alert.alert(
          i18n.t('finish'),
          i18n.t('next_level') + i18n.t('level6'),
          [
            {
              text: i18n.t('cancel'), onPress: () => this.props.navigation.navigate('PracticeMenu')
              , style: 'cancel'
            },
            {text: i18n.t('next'), onPress: () => this.props.navigation.navigate('PracticeLevel6')},
          ],
          {cancelable: false}
        )
      }

    } else {
      if(this.state.progress < 1) {
        Alert.alert(
          i18n.t('wrong'),
          null,
          [
            {text: i18n.t('ok')},
          ],
          {cancelable: false}
        )
      } else {
        if(alert == null) {
          alert = Alert.alert(
            i18n.t('game_over'),
            i18n.t('new_game'),
            [
              {
                text: i18n.t('cancel'), onPress: () => this.props.navigation.navigate('PracticeMenu')
                , style: 'cancel'
              },
              {text: i18n.t('ok'), onPress: () => this.getData()},
            ],
            {cancelable: false}
          )
        }
      }
    }
  }
  onAgain = () => {
    clearInterval(intervalImage)
    clearInterval(intervalProgress)
    this.setState({
      progress : 0,
      image : ''
    })
    this.setViewLevel(index)
  }
  render()
  {
    const {image} = this.state
    return (
      <View style={{flex: 1, width: '100%'}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize : 30,fontWeight: 'bold'}}>{index+1}/{maxLengthData}</Text>
          <Image
            style={{flex: 7, width: '80%', height: '80%'}}
            source={{uri: `https://res.cloudinary.com/dicproyxc/image/upload/v1544609787/${image}.png`}}
            resizeMode="contain"
          />
          <ProgressBarAndroid
            style={{flex: 0.3, width: '80%', justifyContent: 'flex-end'}}
            styleAttr="Horizontal"
            indeterminate={false}
            progress={this.state.progress}
          />
          <View style={{flex: 1.7, width: '100%', flexDirection: 'row'}}>
            <View
              style={ style.bottomView }
            >
              <TextInput
                style={ style.txtCharacter }
                editable={ true }
                value={this.state.dapan}
                onChangeText={(text) => this.setState({dapan : text})}
              >
                { this.state.current }
              </TextInput>
              <View
                style={ style.buttonView }
              >
                <TouchableOpacity
                  style={ style.btnAgain }
                  onPress={ this.onAgain }
                >
                  <Text style={ style.txt }>{ i18n.t('play_again') }</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={ style.btnFinish }
                  onPress={ this.onConfirm }
                >
                  <Text style={ style.txt }>{ i18n.t('next_') }</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default PracticeLevel5