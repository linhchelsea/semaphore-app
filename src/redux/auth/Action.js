import { REGISTER_EMAIL, SET_VERIFY_WARNING, SIGN_UP } from '../../config/actions';
import { registerEmail } from '../../services/AuthService';

export const actionRegisterEmail = (email, isSendCode) => {
  return {
    type: REGISTER_EMAIL,
    email,
    isSendCode,
  }
};

export const actionSetVerifyWarning = (verifyWarning) => {
  console.log(verifyWarning);
  return {
    type: SET_VERIFY_WARNING,
    verifyWarning,
  }
};

export const actionRegisterEmailThunk = (email) => {
  return async (dispatch) => {
    const data = await registerEmail(email);
    console.log(data);
    if (data.status === 200) {
      dispatch(actionRegisterEmail(email, true));
    } else {
      dispatch(actionSetVerifyWarning(data.message));
    }
  };
};
