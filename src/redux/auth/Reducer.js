import {REGISTER_EMAIL, SET_VERIFY_WARNING} from "../../config/actions";

export const defaultState = {
  isSendCode: false,
  email: '',
  verifyWarning: '',
  verifyCodeWarning: '',
  passwordWarning: '',
  passwordConfirmWarning: '',
};

export const authReducer = (state = defaultState, action) => {
  switch (action.type) {
    case REGISTER_EMAIL:
      return { ...state, isSendCode: action.isSendCode, email: action.email };
    case SET_VERIFY_WARNING:
      return { ...state, verifyWarning: action.verifyWarning };
    default:
      return { ...state };
  }
};
