import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import style from "../../style/learnStyle";
import { i18n } from '../../locales/I18n';

export default class Review extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor (props) {
    super(props);
    this.state = {
      data: [],
      current: 'A',
      text: '',
    }
  }

  componentDidMount () {
    const { item } = this.props.navigation.state.params;
    this.setState({ data: item.content }, () => {
      this.setState({ current: this.state.data[0]});
    });
  }

  render () {
    return (
      <View
        style={ style.container }
      >
        <View style={ style.imgView }>
          <Image
            style={ style.imgCharacter }
            source={{ uri: `https://res.cloudinary.com/dicproyxc/image/upload/v1544609787/${this.state.current}.png` || 'ready'}}
          />
        </View>
        <View
          style={ style.bottomView }
        >
          <TextInput
            style={ style.txtCharacter }
            onChangeText={ (text) => this.setState({ text })}
            value={ this.state.text }
          />
          <TouchableOpacity
            style={ style.btnFinish }
            onPress={ this.onContinue }
          >
            <Text style={ style.txt }>{ i18n.t('continue_') }</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  onContinue = () => {
    if (this.state.text.toLowerCase() === this.state.current.toLowerCase()) {
      const currentIndex = this.state.data.indexOf(this.state.current);
      let nextIndex = Math.floor(Math.random() * this.state.data.length);
      while (nextIndex === currentIndex) {
        nextIndex = Math.floor(Math.random() * this.state.data.length);
      }
      const nextCharacter = this.state.data[nextIndex];
      this.setState({current: nextCharacter, text: ''});
    } else {
      Alert.alert(i18n.t('review_'),i18n.t('wrong_answer'),
        [
          {text: i18n.t('again_'), onPress: () => console.log('aaa'), style: 'cancel'},
        ],
        { cancelable: false }
      )
    }
  }
}