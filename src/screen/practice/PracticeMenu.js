import React, { Component } from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import mainMenuStyle from '../../style/mainMenuStyle'
import { i18n } from '../../locales/I18n'

export default class PracticeMenu extends Component {
  static navigationOptions = {
    header: null,
  }

  render () {
    return (
      <View
        style={mainMenuStyle.container}
      >
        <TouchableOpacity
          onPress={this.onLevel1}
          style={mainMenuStyle.btn}>
          <Text style={mainMenuStyle.txtBtn}>{i18n.t('level1')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.onLevel2}
          style={mainMenuStyle.btn}>
          <Text style={mainMenuStyle.txtBtn}>{i18n.t('level2')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.onLevel3}
          style={mainMenuStyle.btn}>
          <Text style={mainMenuStyle.txtBtn}>{i18n.t('level3')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.onLevel4}
          style={mainMenuStyle.btn}>
          <Text style={mainMenuStyle.txtBtn}>{i18n.t('level4')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.onLevel5}
          style={mainMenuStyle.btn}>
          <Text style={mainMenuStyle.txtBtn}>{i18n.t('level5')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.onLevel6}
          style={mainMenuStyle.btn}>
          <Text style={mainMenuStyle.txtBtn}>{i18n.t('level6')}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  onLevel1 = () => {
    this.props.navigation.push('PracticeLevel1', {})
  }

  onLevel2 = () => {
    this.props.navigation.push('PracticeLevel2', {})
  }

  onLevel3 = () => {
    this.props.navigation.push('PracticeLevel3', {})
  }

  onLevel4 = () => {
    this.props.navigation.push('PracticeLevel4', {})
  }

  onLevel5 = () => {
    this.props.navigation.push('PracticeLevel5', {})
  }

  onLevel6 = () => {
    this.props.navigation.push('PracticeLevel6', {})
  }
}