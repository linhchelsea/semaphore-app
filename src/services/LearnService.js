import { API_URL, DEFAULT_RESPONSE } from '../config/global';
import {getToken} from "../stores/authStore";

export const getListLearning = async () => {
  const token = await getToken();
  const response = await fetch(`${API_URL}/api/group/list`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    }
  });
  return response ? JSON.parse(response._bodyInit) : DEFAULT_RESPONSE;
}
