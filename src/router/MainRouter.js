import { createAppContainer, createStackNavigator } from 'react-navigation';
import Login from "../screen/auth/Login";
import MainMenu from "../screen/menu/MainMenu";
import Switch from "../screen/Switch";
import Register from "../screen/auth/Register";
import PracticeLevel1 from "../screen/practice/PracticeLevel1";
import PracticeMenu from "../screen/practice/PracticeMenu";
import Select from "../screen/learn/Select";
import Learn from "../screen/learn/Learn";
import Introduce from "../screen/learn/Introduce";
import Review from "../screen/learn/Review";
import PracticeLevel2 from '../screen/practice/PracticeLevel2'
import PracticeLevel3 from '../screen/practice/PracticeLevel3'
import PracticeLevel4 from '../screen/practice/PracticeLevel4'
import PracticeLevel5 from '../screen/practice/PracticeLevel5'
import PracticeLevel6 from '../screen/practice/PracticeLevel6'
import Game from '../screen/game2player/Game'

const MainRouter = createStackNavigator({
  Main: { screen: Switch },
  Login: { screen: Login },
  Register: { screen: Register },
  Menu: { screen: MainMenu },
  Learn: { screen: Select },
  Review: { screen: Review },
  LearnGroup: { screen: Learn },
  Introduce: { screen: Introduce },
  Game: { screen: Game },
  PracticeLevel1: {screen: PracticeLevel1},
  PracticeLevel2: {screen: PracticeLevel2},
  PracticeLevel3: {screen: PracticeLevel3},
  PracticeLevel4: {screen: PracticeLevel4},
  PracticeLevel5: {screen: PracticeLevel5},
  PracticeLevel6: {screen: PracticeLevel6},
  PracticeMenu: {screen: PracticeMenu}
}, {
  initialRouteName: 'Main',
});

const AppContainer = createAppContainer(MainRouter);

export default AppContainer;
