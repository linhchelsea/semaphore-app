import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import loginStyle from "../../style/loginStyle";
import { signIn } from "../../services/AuthService";
import {setToken} from "../../stores/authStore";
import { i18n } from '../../locales/I18n';

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor (props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      warning: '',
    };

  }
  render () {
    return (
      <View style={ loginStyle.container }>
         <View style={ loginStyle.mainView }>
           <View style={ loginStyle.logoView }>
             <Text style={ loginStyle.title }>{ i18n.t('login_') }</Text>
           </View>
           <View style={ loginStyle.inputView }>
             <Text style={ loginStyle.txtWarning }>{ i18n.t(this.state.warning) }</Text>
             <TextInput
               style={ loginStyle.txtInput }
               placeholder={ i18n.t('enter_your_email') }
               placeholderTextColor={ '#000000' }
               keyboardType={ 'email-address' }
               onChangeText={ (text) => this.setState({ email: text })}
             />
             <TextInput
               style={ loginStyle.txtInput }
               placeholder={ i18n.t('enter_your_password') }
               placeholderTextColor={ '#000000' }
               secureTextEntry={ true }
               onChangeText={ (text) => this.setState({ password: text })}
             />
             <TouchableOpacity
               style={ loginStyle.btnLogin }
               onPress={ this.login }
             >
               <Text style={ loginStyle.txtBtnLogin }>{ i18n.t('login_') }</Text>
             </TouchableOpacity>
           </View>
         </View>
        <View style={ loginStyle.bottomView }>
          <TouchableOpacity>
            <Text style={ loginStyle.txtLink } onPress={ this.redirectRegister }>
              { i18n.t('create_an_account') }
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  redirectRegister = () => {
    this.props.navigation.push('Register');
  }

  login = async () => {
    const data = await signIn({
      email: this.state.email,
      password: this.state.password,
    });
    if (data.status === 200) {
      await setToken(data.data.token);
      this.props.navigation.navigate('Menu');
    } else {
      this.setState({ warning: data.message });
    }
  }
}