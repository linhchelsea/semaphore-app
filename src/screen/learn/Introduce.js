import React, { Component } from 'react';
import { View, WebView } from 'react-native';
import { i18n } from "../../locales/I18n";
const languages = {
  'en-US': 'en',
  'vi-VN': 'vi'
}
export default class Introduce extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor (props) {
    super(props);
    this.state = {
      language: 'en',
    }
  }

  async componentDidMount () {
    const locale = i18n.currentLocale();
    this.setState({ language: languages[locale] });
  }
  render () {
    return (
      <View
        style={{ flex: 1 }}
      >
        <WebView
          source={{ uri: `http://18.216.126.225:3333/introduce_${this.state.language}.html` }}
        />
      </View>
    );
  }
}