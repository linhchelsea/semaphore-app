import { StyleSheet } from 'react-native';
import {getHeight, getWidth} from "../util/Size";

export default mainMenuStyle = StyleSheet.create({
  container: {
    flex: 1,
    width: getWidth(100),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1175a0'
  },
  list: {
    width: getWidth(80),
    height: getHeight(70),
    paddingTop: getHeight(10)
  },
  btn: {
    width: getWidth(80),
    height: getHeight(8),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1175a0',
    borderWidth: 1,
    borderColor: '#ffffff',
    borderRadius: 6,
    marginTop: 4,
    marginBottom: 4,
  },
  txtBtn: {
    color: '#ffffff',
    fontSize: 20,
  },
});