import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import registerStyle from "../../style/registerStyle";
import { i18n } from '../../locales/I18n';

export default class RegisterEmail extends Component {
  state = {
    email: '',
    warning: '',
  };
  constructor (props) {
    super(props);
  }
  render () {
    return (
      <View style={ registerStyle.itemView }>
        <Text style={ registerStyle.txtWarning }>{ this.props.verifyWarning }</Text>
        <TextInput
          style={ registerStyle.txtInput }
          placeholder={ i18n.t('enter_your_email') }
          placeholderTextColor={ '#000000' }
          keyboardType={ 'email-address' }
          onChangeText={ (text) => this.setState({ email: text })}
        />
        <TouchableOpacity
          style={ registerStyle.btn }
          onPress={ this.onSendVerifyCode }
        >
          <Text style={ registerStyle.txtBtnLogin }>{ i18n.t('next_') }</Text>
        </TouchableOpacity>
      </View>
    )
  }

  onSendVerifyCode = async () => {
    if(this.state.email) {
      this.props.registerEmail(this.state.email);
    } else {
      this.props.setVerifyWarning(i18n.t('email_is_required'));
    }
  }
}