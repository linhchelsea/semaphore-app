import React, {Component} from 'react';
import {View, Picker, StyleSheet, TouchableOpacity, Text, TextInput, Image} from 'react-native';

class Practice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            language: "",
            textInput: "50"
        }
    }

    onSubtractionClick = () => {
        this.setState((prevState) => ({
            textInput: prevState.textInput - 5
        }));
    };
    onAdditionClick = () => {
        this.setState((prevState) => ({
            textInput: prevState.textInput + 5
        }));
    };
    onPlayClick = () => {
      alert("onPlayClick")
    };
    onRenewClick = () => {
        alert("onRenewClick")
    };
    onCheckClick = () => {
        alert("onCheckClick")
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{
                    flex: 1,
                    flexDirection: "row",
                    backgroundColor: "#39f0ff",
                    alignItems: "center",
                    justifyContent: "space-around"
                }}>
                    <Picker
                        selectedValue={this.state.language}
                        style={{height: 50, width: "60%"}}
                        onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                        <Picker.Item label="Random Text Only" value="text_only"/>
                        <Picker.Item label="Random Text and Number" value="text_and_number"/>
                        <Picker.Item label="Real World Content" value="Real"/>
                    </Picker>
                    <TouchableOpacity
                        onPress={this.onSubtractionClick}
                        style={{
                            width: 50,
                            height: 50,
                            justifyContent: "center",
                            alignItems: "center",
                            borderColor: "#fff",
                            borderWidth: 2
                        }}>
                        <Text> - </Text>
                    </TouchableOpacity>
                    <TextInput
                        style={{
                            width: 60, height: 50, borderColor: 'gray', borderWidth: 1,
                            justifyContent: "center", alignItems: "center", textAlign: "center"
                        }}
                        keyboardType='numeric'
                        // onChangeText={(text) => this.setState({textInput: text})}
                        value={this.state.textInput.toString()}
                        placeholder={"CMP"}
                    />
                    <TouchableOpacity
                        onPress={this.onAdditionClick}
                        style={{
                            width: 50,
                            height: 50,
                            justifyContent: "center",
                            alignItems: "center",
                            borderColor: "#fff",
                            borderWidth: 2
                        }}>
                        <Text> + </Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 9, backgroundColor: "#fff"}}>
                    <View style={{flex: 8, alignItems: "center", justifyContent: "center"}}>
                        <Image
                            style={{width: "80%", height: "80%"}}
                            source={require('../../../assets/images/flags/D.png')}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{flex: 2}}>
                        <View style={styles.view_bottom}>
                            <TouchableOpacity
                                onPress={this.onPlayClick}
                                style={styles.button_bottom}>
                                <Text> Play </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={this.onRenewClick}
                                style={styles.button_bottom}>
                                <Text> Renew </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={this.onCheckClick}
                                style={styles.button_bottom}>
                                <Text> Check </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 1, width: "100%", flexDirection: "row"}}>
                            <TextInput style={{width: "100%", paddingHorizontal: 10}}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%"
    },
    welcome: {
        fontSize: 20
    },
    view_bottom : {
        flex: 1,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around"
    },
    button_bottom : {
        width: "30%",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#39f0ff",
        borderWidth: 2
    }
});

export default Practice;