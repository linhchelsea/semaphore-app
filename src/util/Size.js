import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');

export const getWidth = percent => width * percent / 100;
export const getHeight = percent => height * percent / 100;
