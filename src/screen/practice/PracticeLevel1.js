import React, { Component } from 'react'
import { View, Text, Image, ProgressBarAndroid, TouchableOpacity, Alert } from 'react-native'
import { getPractice } from '../../services/AuthService'
import { i18n } from '../../locales/I18n'

let listData = []
let maxLengthData = 0
let index = 0
const chars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
let interval = null
let alert = null

class PracticeLevel1 extends Component {
  static navigationOptions = {title: i18n.t('level1')}

  constructor (props) {
    super(props)
    this.state = {
      image: '',
      button1: '',
      button2: '',
      progress: 0,
      dapan: ''
    }
  }

  componentDidMount () {
    listData = []
    maxLengthData = 0
    index = 0
    this.getData()
  }

  getData = async () => {
    this.setState({
      progress: 0
    })
    const data = await getPractice({
      level: 1,
    })
    if (data.status === 200) {
      listData = data.data
      maxLengthData = listData.length
      index = 0
      this.setViewLevel(index)
      console.log(listData)
    } else {
      alert('error')
    }
  }

  setViewLevel = (level) => {
    if (listData.length !== 0) {
      this.setState({
        image: listData[level].text
      }, () => {
        this.get(this.state.image)
        console.log(this.state.image)
      })
      interval = setInterval(() => {
        var progress = (this.state.progress + 0.00666666666)
        if (this.state.progress <= 1) {
          this.setState({progress: progress})
        } else {
          clearInterval(interval)
          if (alert == null) {
            alert = Alert.alert(
              i18n.t('game_over'),
              i18n.t('new_game'),
              [
                {
                  text: i18n.t('cancel'), onPress: () => {
                    this.props.navigation.navigate('PracticeMenu')
                    alert = null
                  }
                  , style: 'cancel'
                },
                {
                  text: i18n.t('ok'), onPress: () => {
                    this.getData()
                    alert = null
                  }
                },
              ],
              {cancelable: false}
            )
          }
        }
      })

    }

  }
  get = (char) => {
    const indexChar = chars.indexOf(char)
    let index = Math.floor(Math.random() * chars.length)
    while (index === indexChar) {
      index = Math.floor(Math.random() * chars.length)
    }
    const char2 = chars[index]
    const random = Math.floor(Math.random() * 2)
    const result = random ? [char, char2] : [char2, char]
    this.setState({
      button1: result[0].toString(),
      button2: result[1].toString(),
    })
  }

  onConfirm = () => {
    if (this.state.dapan === this.state.image) {
      if (index < maxLengthData - 1) {
        index++
        clearInterval(interval)
        this.setState({
          progress: 0,
          dapan: '',
          image: ''
        })
        this.setViewLevel(index)
      } else {
        Alert.alert(
          i18n.t('finish'),
          i18n.t('next_level') + i18n.t('level2'),
          [
            {
              text: i18n.t('cancel'), onPress: () => this.props.navigation.navigate('PracticeMenu')
              , style: 'cancel'
            },
            {text: i18n.t('next'), onPress: () => this.props.navigation.navigate('PracticeLevel2')},
          ],
          {cancelable: false}
        )
      }

    } else {
      if (alert == null) {
        alert = Alert.alert(
          i18n.t('game_over'),
          i18n.t('new_game'),
          [
            {
              text: i18n.t('cancel'), onPress: () => this.props.navigation.navigate('PracticeMenu')
              , style: 'cancel'
            },
            {text: i18n.t('ok'), onPress: () => this.getData()},
          ],
          {cancelable: false}
        )
      }
    }
  }

  pressButton1 = () => {
    clearInterval(interval)
    this.setState({
      dapan: this.state.button1
    }, () => {
      this.onConfirm()
    })
  }
  pressButton2 = () => {
    clearInterval(interval)
    this.setState({
      dapan: this.state.button2
    }, () => {
      this.onConfirm()
    })
  }

  render () {
    const {image} = this.state
    return (
      <View style={{flex: 1, width: '100%'}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 30, fontWeight: 'bold'}}>{index + 1}/{maxLengthData}</Text>
          <Image
            style={{flex: 7, width: '80%', height: '80%'}}
            source={{uri: `https://res.cloudinary.com/dicproyxc/image/upload/v1544609787/${image}.png`}}
            resizeMode="contain"
          />
          <ProgressBarAndroid
            style={{flex: 0.3, width: '80%', justifyContent: 'flex-end'}}
            styleAttr="Horizontal"
            indeterminate={false}
            progress={this.state.progress}
          />
          <View style={{flex: 1.7, width: '100%', flexDirection: 'row'}}>
            <TouchableOpacity
              style={{
                width: '50%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ffee70'
              }}
              onPress={this.pressButton1}>

              <Text style={{fontSize: 30}}> {this.state.button1} </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: '50%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#ff001c'
              }}
              onPress={this.pressButton2}>
              <Text style={{fontSize: 30}}> {this.state.button2} </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

export default PracticeLevel1