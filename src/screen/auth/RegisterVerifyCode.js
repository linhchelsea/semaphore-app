import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { i18n } from "../../locales/I18n";
import registerStyle from "../../style/registerStyle";
import {signUp} from "../../services/AuthService";

export default class RegisterVerifyCode extends Component {
  constructor (props) {
    super(props);
    this.state = {
      code: '',
      password: '',
      password_confirm: '',
      warning: '',
    };
  }

  render () {
    return (
      <View style={ registerStyle.itemView }>
        <Text style={ registerStyle.txtWarning }>{ i18n.t(this.state.warning) }</Text>
        <TextInput
          style={ registerStyle.txtInput }
          placeholder={ i18n.t('enter_verify_code') }
          placeholderTextColor={ '#000000' }
          onChangeText={ (text) => this.setState({ code: text }) }
        />
        <TextInput
          style={ registerStyle.txtInput }
          placeholder={ i18n.t('enter_your_password') }
          placeholderTextColor={ '#000000' }
          secureTextEntry={ true }
          onChangeText={ (text) => this.setState({ password: text }) }
        />
        <TextInput
          style={ registerStyle.txtInput }
          placeholder={ i18n.t('confirm_password') }
          placeholderTextColor={ '#000000' }
          secureTextEntry={ true }
          onChangeText={ (text) => this.setState({ password_confirm: text }) }
        />
        <TouchableOpacity
          style={ [registerStyle.btn, { marginTop: 20 }] }
          onPress={ this.onRegister }
        >
          <Text style={ registerStyle.txtBtnLogin }>{ i18n.t('register_') }</Text>
        </TouchableOpacity>
      </View>
    );
  }

  onRegister = async () => {
    const data = await signUp({
      email: this.props.email,
      code: this.state.code,
      password: this.state.password,
      passwordConfirm: this.state.password_confirm,
    });
    if (data.status === 200) {
      this.props.navigation.navigate('Login');
    } else {
      this.setState({ warning: data.message });
    }
  }
}
