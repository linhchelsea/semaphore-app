import { StyleSheet } from 'react-native';
import {getHeight, getWidth} from "../util/Size";

export default loginStyle = StyleSheet.create({
  container: {
    width: getWidth(100),
    height: getHeight(50)
  },
  imgView: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnFinish: {
    width: getWidth(50),
    height: getHeight(8),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
  },
  txt: {
    color: '#ffffff',
    fontSize: 20,
  },
  txtHint: {
    fontSize: 15,
    color: '#6c6c6c',
  },
  txtCharacter: {
    width: getWidth(50),
    height: getHeight(6),
    borderWidth: 1,
    borderColor: 'black',
    textAlign: 'center',
    marginTop: 8,
    marginBottom: 8,
  },
  imgCharacter: {
    width: getWidth(50),
    height: getWidth(50),
  },
});