import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { authReducer } from './auth/Reducer';

const reducer = combineReducers({
  auth: authReducer,
});

export const store = createStore(reducer, applyMiddleware(thunk));
