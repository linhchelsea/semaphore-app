import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text, TextInput, Alert } from 'react-native';
import style from "../../style/learnStyle";
import { i18n } from '../../locales/I18n';

export default class Learn extends Component {
  constructor (props) {
    super(props);
    this.state = {
      data: [],
      current: '',
    }
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount () {
    const { item } = this.props.navigation.state.params;
    this.setState({ data: item.content }, () => {
      this.setState({ current: this.state.data[0]});
    });
  }

  render () {
    return (
      <View
        style={ style.container }
      >
        <View style={ style.imgView }>
          <Image
            style={ style.imgCharacter }
            source={{ uri: `https://res.cloudinary.com/dicproyxc/image/upload/v1544609787/${this.state.current}.png` || 'ready'}}
          />
        </View>
        <View
          style={ style.bottomView }
        >
          <Text style={ style.txtHint }>{ i18n.t('press_next_button_if_you_understood_this_character') }</Text>
          <TextInput
            style={ style.txtCharacter }
            editable={ false }
          >
            { this.state.current }
          </TextInput>
          <TouchableOpacity
            style={ style.btnFinish }
            onPress={ this.onNextCharacter }
          >
            <Text style={ style.txt }>{ i18n.t('next_') }</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  onNextCharacter = () => {
    const index = this.state.data.indexOf(this.state.current);
    const nextIndex = index < this.state.data.length - 1 ? index + 1 : 0;
    if (nextIndex === 0) {
      Alert.alert(i18n.t('review_'),i18n.t('do_you_want_to_review_this_group'),
        [
          {text: i18n.t('no_'), onPress: () => this.props.navigation.goBack(), style: 'cancel'},
          {text: i18n.t('yes_'), onPress: () => this.props.navigation.navigate('Review', { item: this.props.navigation.state.params.item })},
        ],
        { cancelable: false }
      )
    } else {
      this.setState({current: this.state.data[nextIndex]});
    }
  }
}